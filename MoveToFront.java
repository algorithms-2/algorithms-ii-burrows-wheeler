/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

import java.util.LinkedList;

public class MoveToFront {

    private static final int R = 256;

    // apply move-to-front encoding, reading from standard input and writing to standard output
    public static void encode() {
        LinkedList<Character> moveToFront = moveToFront();
        while (!BinaryStdIn.isEmpty()) {
            char c = BinaryStdIn.readChar();
            BinaryStdOut.write(moveToFront.indexOf(c), 8);
            moveToFront.remove((Character) c);
            moveToFront.addFirst(c);
        }
        BinaryStdOut.close();
    }

    // apply move-to-front decoding, reading from standard input and writing to standard output
    public static void decode() {
        LinkedList<Character> moveToFront = moveToFront();
        while (!BinaryStdIn.isEmpty()) {
            int index = BinaryStdIn.readChar();
            char c = moveToFront.get(index);
            BinaryStdOut.write(c);
            moveToFront.remove((Character) c);
            moveToFront.addFirst(c);
        }
        BinaryStdOut.close();
    }

    private static LinkedList<Character> moveToFront() {
        LinkedList<Character> mtf = new LinkedList<>();
        for (int i = 0; i < R; i++) {
            mtf.add((char) i);
        }
        return mtf;
    }

    // if args[0] is "-", apply move-to-front encoding
    // if args[0] is "+", apply move-to-front decoding
    public static void main(String[] args) {
        encode();
        if (args[0].equals("-")) {
            encode();
        }
        else if (args[0].equals("+")) {
            decode();
        }
    }
}
