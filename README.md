# Algorithms II Assignment 4: Boggle
Solution for assignment 5 of the Princeton University Algorithms II course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/burrows/specification.php).

The goal of this assignment was to implement two of the three parts of the Burrows-Wheeler compression scheme. The class BurrowsWheeler.java performs Burrows-Wheeler transformations on strings or binary data, while MoveToFront.java performs move-to-front encoding and decoding on similar data. This provides a good setup for Huffman compression, the class for which is provided by the course staff.
CircularSuffixArray.java is a utility class used by BurrrowsWheeler.java to get a memory-efficient representation of the circular suffixes of any input string.

## Results  

Correctness:  71/73 tests passed

Memory:       10/10 tests passed

Timing:       159/163 tests passed

<b>Aggregate score:</b> 97.87%


## How to use
See the above assignemnt description for how to use these classes in conjunction with starter code to compress and decompress text and binary files.
<b>NOTE:</b> Depends on the algs4 library used in the course.
