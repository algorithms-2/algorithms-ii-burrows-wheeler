/* *****************************************************************************
 *  Name:
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import edu.princeton.cs.algs4.Queue;

import java.util.HashMap;

public class BurrowsWheeler {
    
    private static final int R = 256;

    // apply Burrows-Wheeler transform,
    // reading from standard input and writing to standard output
    public static void transform() {
        String s = BinaryStdIn.readString();
        BinaryStdIn.close();
        int n = s.length();
        CircularSuffixArray csa = new CircularSuffixArray(s);
        int first = -1;
        StringBuilder transformed = new StringBuilder();
        for (int i = 0; i < n; i++) {
            int csaIndex = csa.index(i);
            if (csaIndex == 0) {
                transformed.append(s.charAt(n - 1));
                first = i;
            }
            else {
                transformed.append(s.charAt(csaIndex - 1));
            }
        }
        BinaryStdOut.write(first);
        BinaryStdOut.write(transformed.toString());
        BinaryStdOut.close();

    }

    // apply Burrows-Wheeler inverse transform,
    // reading from standard input and writing to standard output
    public static void inverseTransform() {
        // input
        int firstSuffix = BinaryStdIn.readInt();
        char[] lastColumn = BinaryStdIn.readString().toCharArray();
        int N = lastColumn.length;
        BinaryStdIn.close();

        // Sort lastColumn[] with key-indexed counting to deduce first column of the suffix matrix,
        // and add the occurrence indexes of each char to a map of queues.
        char[] firstColumn = new char[N];
        HashMap<Character, Queue<Integer>> indexesInLastColumn = new HashMap<>();
        int[] auxCount = new int[R + 1];
        for (int i = 0; i < N; i++) {
            auxCount[lastColumn[i] + 1]++;
            if (indexesInLastColumn.get(lastColumn[i]) == null)
                indexesInLastColumn.put(lastColumn[i], new Queue<>());
            indexesInLastColumn.get(lastColumn[i]).enqueue(i);
        }
        for (int r = 0; r < R; r++) {
            auxCount[r + 1] += auxCount[r];
        }
        for (int i = 0; i < N; i++) {
            firstColumn[auxCount[lastColumn[i]]++] = lastColumn[i];
        }

        // create the "next" array from the information gathered above
        int[] next = new int[N];
        for (int i = 0; i < N; i++) {
            next[i] = indexesInLastColumn.get(firstColumn[i]).dequeue();
        }

        // reconstruct the input from first int and next[]
        StringBuilder output = new StringBuilder();
        int i = firstSuffix;
        while (output.length() != N) {
            output.append(firstColumn[i]);
            i = next[i];
        }
        BinaryStdOut.write(output.toString());
        BinaryStdOut.close();
    }


    // if args[0] is "-", apply Burrows-Wheeler transform
    // if args[0] is "+", apply Burrows-Wheeler inverse transform
    public static void main(String[] args) {
        if (args[0].equals("-")) {
            transform();
        }
        else if (args[0].equals("+")) {
            inverseTransform();
        }
    }

}
