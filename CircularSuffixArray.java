import java.util.Arrays;

public class CircularSuffixArray {

    private final int strLength;
    private final int[] index;

    private static class CircularSuffix
            implements Comparable<CircularSuffix> {
        private final String str;
        private final int startIndex;

        public CircularSuffix(String str, int startIndex) {
            this.startIndex = startIndex;
            this.str = str;
        }

        public int startIndex() {
            return startIndex;
        }

        public int compareTo(CircularSuffix that) {
            int currThis = this.startIndex, currThat = that.startIndex, strlength = str.length();
            while (str.charAt(currThis) == str.charAt(currThat)) {
                if (currThis == this.startIndex - 1 || currThat == that.startIndex - 1) break;
                currThis = (currThis + 1) % strlength;
                currThat = (currThat + 1) % strlength;
            }
            return str.charAt(currThis) - str.charAt(currThat);
        }
    }

    // circular suffix array of s
    public CircularSuffixArray(String s) {
        if (s == null) throw new IllegalArgumentException();
        this.strLength = s.length();
        CircularSuffix[] circularSuffixes = new CircularSuffix[strLength];
        this.index = new int[strLength];
        for (int i = 0; i < strLength; i++) {
            circularSuffixes[i] = new CircularSuffix(s, i);
        }
        Arrays.sort(circularSuffixes);
        for (int i = 0; i < circularSuffixes.length; i++) {
            this.index[i] = circularSuffixes[i].startIndex();
        }
    }

    // length of s
    public int length() {
        return strLength;
    }

    // returns index of ith sorted suffix
    public int index(int i) {
        if (i < 0 || i > strLength - 1) throw new IllegalArgumentException();
        return index[i];
    }

    // unit testing (required)
    public static void main(String[] args) {
        String s = "ABRACADABRA!";
        CircularSuffixArray csa = new CircularSuffixArray(s);
        System.out.println(csa.length());
        for (int i = 0; i < s.length(); i++) {
            System.out.println(csa.index(i));
        }
    }

}